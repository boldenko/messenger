package boldenko.Client;

import boldenko.Core.Message.*;
import boldenko.Core.Net.BinaryProtocol;
import boldenko.Core.Net.ProtocolException;
import boldenko.Core.Net.Protocol;
import boldenko.Core.Storage.Chat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Client {
    private List<Chat> chats;

    private final Logger log = LoggerFactory.getLogger(Client.class);

    private String host = "localhost";
    private int port = 5454;

    private InputStream in;
    private OutputStream out;

    private Protocol protocol;

    private Thread socketInputThread;

    public void establishConnection() throws IOException {
        Socket socket = new Socket(host, port);
        in = socket.getInputStream();
        out = socket.getOutputStream();

        log.info("Successfully connected to server " + host + port);

        socketInputThread = new Thread(() -> {
            byte[] buf = new byte[32*1024];
            int read;

            while (!Thread.currentThread().isInterrupted()) {
                try {
                    read = in.read(buf);

                    if (read > 0) {
                        log.info("Recieved new message");
                        Message msg = protocol.decode(buf);
                        handleRespond(msg);
                    } else
                        if (read == -1) {
                            log.error("Server connection lost.");
                            // допилить попытку релога
                            close();
                            Thread.currentThread().interrupt();
                            break;
                        }
                } catch (Exception e) {
                    log.error(e.toString());
                }
            }
        });

        socketInputThread.start();
    }

    public void handleQuery(String msg) throws IOException {
        // Todo: распарсить запрос, сформировать Message и отправить серверу

        if (msg.length() == 0)
            return;

        //log.info("(query): \"" + msg + "\"");
        if (msg.charAt(0) != '/') {
            // Default behaviour
            Message message = new TextMessage(msg, 0);
            send(message);
            return;
        }

        String[] token = msg.split(" ");
        Message message;

        switch (token[0]) {
            case "/text": {
                long chatId = Long.parseLong(token[1]);
                String text = "";
                for (int i = 2; i < token.length; i++)
                    text += token[i] + ' ';
                message = new TextMessage(text, chatId);
                send(message);
                break;
            }

            case "/q":
                close();
                System.exit(0);
                break;

            case "/login":
                if (token.length != 3) {
                    System.out.print("Invalid input.");
                    break;
                }
                message = new LoginMessage(token[1], token[2]);
                send(message);
                break;

                    // /newchat Job 1 2 3 4
                case "/newchat":
                    if (token.length < 3) {
                    System.out.println("Invalid input");
                    break;
                }

                String title = token[1];
                List<Long> participants = new ArrayList<>();
                for (int i = 2; i < token.length; i++)
                    participants.add(Long.parseLong(token[i]));
                message = new ChatCreateMessage(title, participants);
                send(message);
                break;

            case "/register":
                if (token.length != 3) {
                    System.out.println("Invalid input.");
                    break;
                }
                send(new RegisterMessage(token[1], token[2]));

            case "/chatlist":
                send(new ChatListRequestMessage());
                break;

            case "/history": {
                long chatId = Long.parseLong(token[1]);
                for (Chat chat : chats)
                    if (chat.id() == chatId) {
                        System.out.println(chat.title() + "[id:"+chat.id()+"]");
                        for (TextMessage m : chat.history())
                            System.out.println("["+m.getSenderId()+"]: " + m.getText());
                    }
                break;
            }


            case "/chatinfo":


            default:
                System.out.println("<System> Unknown command.");
                break;
        }
    }

    public void send(Message msg) throws IOException{
        try {
            out.write(protocol.encode(msg));
            out.flush();
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        socketInputThread.interrupt();
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleRespond(Message msg) {
        switch (msg.getType()) {
            case MSG_TEXT:
                long chatId = ((TextMessage)msg).getChatId();
                for (Chat chat : chats)
                    if (chat.id() == chatId) {
                        chat.addMessage((TextMessage)msg);
                        System.out.println(chat.title()+"["+chatId+"] " +
                                chat.memberAlias(msg.getSenderId()) + "[id " + msg.getSenderId() +"]: "
                        + ((TextMessage) msg).getText());
                        break;
                    }
                break;

            case MSG_STATUS:
                switch (((StatusMessage)msg).code()) {
                    case CHAT_INVITE:
                        Chat chat = ((ChatInviteMessage)msg).getChat();
                        System.out.println("[System] You have been invited to chat " +
                                chat.title() + "[id " + chat.id()+ "]");
                        chats.add(chat);
                        break;
                }
                break;

            case MSG_CHAT_LIST:
                chats = ((ChatListMessage)msg).chats();
                System.out.println("Your chats:");
                for (Chat chat : chats)
                    System.out.println("["+chat.id()+"] " + chat.title());
                break;
        }
    }

    public static void main(String[] args) {
        Client client = new Client();
        client.setProtocol(new BinaryProtocol());

        try {
            client.establishConnection();

            Scanner scanner = new Scanner(System.in);
            while (true) {
                String input = scanner.nextLine();
                // Обработать ввод через processInput()
                client.handleQuery(input);
            }
        } catch (IOException e) {
            System.out.println("Connection with server lost. Terminating...");
        } finally {
            if (client != null)
                client.close();
        }
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }
}
