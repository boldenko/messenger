package boldenko.Server;

import boldenko.Command.*;
import boldenko.Core.Message.Message;
import boldenko.Core.Net.BinaryProtocol;
import boldenko.Core.Net.Protocol;
import boldenko.Core.Session;
import boldenko.Core.Storage.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    final Logger log = LoggerFactory.getLogger(Server.class);

    private int port;
    private ServerSocket serverSocket;
    private Set<Session> activeSessions = new HashSet<>();
    private Protocol protocol;

    private ExecutorService threadPool;

    private UserManager userStorage = new UserStorage();
    private ChatManager chatStorage = new ChatStorage();

    private CommandExecutor executor;

    public Server(int port) throws ServerFormationException{
        this.port = port;
        protocol = new BinaryProtocol();
        threadPool = Executors.newCachedThreadPool();
        executor = new CommandExecutor();
        executor.add(Message.Type.MSG_TEXT, new TextCommand(this));
        executor.add(Message.Type.MSG_LOGIN, new LoginCommand(this));
        executor.add(Message.Type.MSG_CHAT_CREATE, new ChatCreateCommand(this));
        executor.add(Message.Type.MSG_REGISTER, new RegisterCommand(this));
        executor.add(Message.Type.MSG_CHAT_LIST_REQUEST, new ChatListCommand(this));
        // Todo: add other commands to executor



        try {
            List<User> party = new ArrayList<>();
            party.add(userStorage.registerUser("admin", "pass"));
            party.add(userStorage.registerUser("bill", "alamo"));

            chatStorage.createChat("Job", party);
        } catch (Exception e) {}

        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            log.error("Server not created. Unable to open socket.\n"+e.toString());
            throw new ServerFormationException("Server not created. Unable to open socket.\n"+e.toString());
        }
        log.info("Server successfully started.");
    }

    public void roll() {
        Socket client;
        Session session;

        while (!Thread.currentThread().isInterrupted())
            try {
                client = serverSocket.accept();
                session = new Session(this, client, protocol, executor);
                activeSessions.add(session);
                threadPool.submit(session);

            } catch (IOException e) {
                // если IOException из accept сработало, нужно ли дальше слушать этот сокет?
                log.error("Failed to accept new connection.");
            }

        this.shut();
    }

    public void shut() {
        log.info("Shutting down server.");
        threadPool.shutdown();

        // Server interrupted, closing everything
    }



    public Set<Session> getActiveSessions() {
        return activeSessions;
    }

    public void removeSession(Session session) {
        activeSessions.remove(session);
    }

    public static void main(String[] args) {
        Server server;
        try {
            server = new Server(5454);
        } catch (ServerFormationException sfe) {
            return;
        }
        server.roll();
    }

    public ChatManager getChatStorage() {
        return chatStorage;
    }

    public UserManager getUserStorage() {
        return userStorage;
    }
}
