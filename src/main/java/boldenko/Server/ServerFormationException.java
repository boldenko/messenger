package boldenko.Server;

public class ServerFormationException extends Exception {
    public ServerFormationException(String info) {
        super(info);
    }
}
