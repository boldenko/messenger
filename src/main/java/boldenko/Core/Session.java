package boldenko.Core;

import boldenko.Command.CommandException;
import boldenko.Command.CommandExecutor;
import boldenko.Core.Message.Message;
import boldenko.Core.Message.StatusMessage;
import boldenko.Core.Net.ConnectionHandler;
import boldenko.Core.Net.Protocol;
import boldenko.Core.Net.ProtocolException;
import boldenko.Core.Storage.User;
import boldenko.Server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class Session implements ConnectionHandler, Runnable {
    private final Logger log = LoggerFactory.getLogger(Session.class);

    private User user;
    private Socket socket;
    private Server server;

    private InputStream in;
    private OutputStream out;

    private Protocol protocol;

    private CommandExecutor executor;

    public Session(Server server, Socket socket, Protocol protocol, CommandExecutor executor) throws IOException{
        this.server = server;
        this.socket = socket;
        this.in = socket.getInputStream();
        this.out = socket.getOutputStream();
        user = null;
        this.protocol = protocol;
        this.executor = executor;
    }

    @Override
    public void run() {
        byte[] buf = new byte[32*1024];

        log.info("New connection from " +socket.getInetAddress().toString());

        while (!Thread.currentThread().isInterrupted()) {
            try {
                int read = in.read(buf);
                if (read > 0) {
                    Message msg = protocol.decode(buf);
                    msg.setTimestamp(System.currentTimeMillis());
                    if (user != null)
                        msg.setSenderId(user.id());

                    onMessage(msg);
                } else if (read == -1) {
                    log.info("User " + getAddr() + " disconnected");
                    close();
                    Thread.currentThread().interrupt();
                }
            } catch (IOException e) {
                log.error(e.getMessage());
                Thread.currentThread().interrupt();
            } catch (ProtocolException e) {
                log.error(e.getMessage());
                // Todo return bad status message
            }
        }
    }

    @Override
    public void send(Message msg)  {
        try {
            out.write(protocol.encode(msg));
            out.flush();
        } catch (ProtocolException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void onMessage(Message msg) {
        try {
            executor.handle(msg, this);
        } catch (CommandException ce) {
            Message response = new StatusMessage(StatusMessage.StatusCode.BAD_COMMAND, ce.getMessage());
            try {
                send(response);
            } catch (Exception e) {
                close();
            }
        }
    }

    @Override
    public void close() {
        try {
            in.close();
        } catch (IOException e) {}

        try {
            out.close();
        } catch (IOException e) {}

        try {
            socket.close();
        } catch (IOException e) {}
        // interrupt ?
        server.removeSession(this);
    }

    public User user() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public InetAddress getAddr() {
        return socket.getInetAddress();
    }
}
