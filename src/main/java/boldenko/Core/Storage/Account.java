package boldenko.Core.Storage;

import java.util.Objects;

public class Account {
    private long id;
    private String login;
    private String password;
    private String alias;

    public Account(long id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.alias = login;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long id() {
        return id;
    }

    public String login() {return login;}

    public String password() {return password;}

    public String toString() {
        return "[id: " + id + ", login: " + login + ", password: " + password + ", alias: " + alias +"]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                login.equals(account.login) &&
                password.equals(account.password) &&
                Objects.equals(alias, account.alias);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, alias);
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String alias() {
        return alias;
    }
}
