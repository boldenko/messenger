package boldenko.Core.Storage;

public class UserNotFoundException extends Exception {
    public UserNotFoundException() {}
    public UserNotFoundException(String info) {
        super(info);
    }
}
