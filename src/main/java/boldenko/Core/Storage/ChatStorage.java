package boldenko.Core.Storage;

import java.util.ArrayList;
import java.util.List;

public class ChatStorage implements ChatManager{
    private long counter = 0;
    private List<Chat> chats = new ArrayList<>();

    // Assume that participants are already validated and without repetitions
    // Can I assume that ?
    public Chat createChat(String title, List<User> participants) {
        Chat newChat = new Chat(counter++, title);
        newChat.setParticipants(participants);
        chats.add(newChat);
        return newChat;
    }

    @Override
    public Chat chat(long id){
        for (Chat chat : chats)
            if (chat.id() == id)
                return chat;

        return null;
    }

    @Override
    public List<Chat> userChats(long userId) {
        List<Chat> userChats = new ArrayList<>();

        for (Chat chat : chats)
            if (chat.isMember(userId))
                userChats.add(chat);

        return userChats;
    }
}
