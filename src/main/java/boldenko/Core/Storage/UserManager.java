package boldenko.Core.Storage;

import java.util.List;

public interface UserManager {
    User user(long userId);

    User login(String login, String password) throws LoginException, UserNotFoundException;

    User registerUser(String login, String password) throws LoginOccupiedException;

    void setAlias(long id, String alias);

    List<User> validate(List<Long> party);
}
