package boldenko.Core.Storage;

import boldenko.Core.Message.TextMessage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Chat implements Serializable{
    private long id;
    private String title;
    private List<User> participants = new ArrayList<>();
    private List<TextMessage> history = new ArrayList<>();

    public Chat(long id, String title) {
        this.id = id;
        this.title = title;
    }

    // synchronized ?
    public void addMessage(TextMessage msg) {
        history.add(msg);
    }

    public void removeParticipant(User user) {
        for (User u: participants)
            if (u.id() == user.id()) {
                participants.remove(u);
                break;
            }
    }

    public long id() {
        return id;
    }

    public String title() {
        return title;
    }

    public List<TextMessage> history() {return history;}

    public List<User> participants() {
        return participants;
    }

    public boolean isMember(long userId) {
        for (User user : participants)
            if (user.id() == userId)
                return true;
            return false;
    }

    public String memberAlias(long userId) {
        for (User user : participants)
            if (user.id() == userId)
                return user.alias();

            return "";
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setParticipants(List<User> party) {
        this.participants = party;
    }

    public void addParticipant(User user) {
        for (User u : participants)
            if (u.id() == user.id())
                return;

        participants.add(user);
    }
}
