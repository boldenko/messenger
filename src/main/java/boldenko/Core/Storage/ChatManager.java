package boldenko.Core.Storage;

import java.util.List;

public interface ChatManager {
    Chat createChat(String title, List<User> participants);

    Chat chat(long id);

    List<Chat> userChats(long userId);
}
