package boldenko.Core.Storage;

import java.util.ArrayList;
import java.util.List;

public class UserStorage implements UserManager{
    private int counter = 0;
    private List<Account> accounts = new ArrayList<>();

    @Override
    public User user(long userId) {
        for (Account account : accounts)
            if (account.id() == userId)
                return new User(account.id(), account.alias());
        return null;
    }

    @Override
    public User login(String login, String password) throws LoginException, UserNotFoundException {
        for (Account account : accounts)
            if (account.login().equals(login))
                if (account.password().equals(password))
                    return new User(account.id(), account.alias());
                else
                    throw new LoginException("Incorrect password.");
        throw new UserNotFoundException("User [" + login + "] not found.");
    }

    @Override
    public User registerUser(String login, String password) throws LoginOccupiedException {
        for (Account account : accounts)
            if (account.login().equals(login))
                throw new LoginOccupiedException();

        Account account = new Account(counter++, login, password);
        accounts.add(account);
        return new User(account.id(), account.alias());
    }

    @Override
    public void setAlias(long id, String alias) {
        for (Account account : accounts)
            if (account.id() == id)
                account.setAlias(alias);
    }

    @Override
    public List<User> validate(List<Long> party) {
        List<User> validated = new ArrayList<>();
        for (Long userId : party)
            if ((user(userId) != null) && !validated.contains(userId))
                validated.add(user(userId));

        return validated;
    }
}
