package boldenko.Core.Storage;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {
    private long id;
    private String alias;

    public User(long id, String alias) {
        this.id = id;
        this.alias = alias;
    }

    public long id() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String alias() {
        return alias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                alias.equals(user.alias);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, alias);
    }
}
