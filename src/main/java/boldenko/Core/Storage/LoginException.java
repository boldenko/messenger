package boldenko.Core.Storage;

public class LoginException extends Exception {
    public LoginException() {}
    public LoginException(String info) {
        super(info);
    }
}
