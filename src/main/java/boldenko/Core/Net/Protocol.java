package boldenko.Core.Net;

import boldenko.Core.Message.Message;

public interface Protocol {

    Message decode(byte[] bytes) throws ProtocolException;

    byte[] encode(Message msg) throws ProtocolException;

}
