package boldenko.Core.Net;

public class ProtocolException extends Exception {
    public ProtocolException() {}

    public ProtocolException(Exception cause) {
        super(cause);
    }

    public ProtocolException(String info, Exception cause) {
        super(info, cause);
    }
}
