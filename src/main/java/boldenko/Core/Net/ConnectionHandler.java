package boldenko.Core.Net;

import boldenko.Core.Message.Message;

public interface ConnectionHandler {
    void send(Message msg);
    void onMessage(Message msg);
    void close();
}
