package boldenko.Core.Net;

import boldenko.Core.Message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class BinaryProtocol implements Protocol {
    private final Logger log = LoggerFactory.getLogger(BinaryProtocol.class);

    @Override
    public Message decode(byte[] bytes) throws ProtocolException {
        Message msg = null;

        try (ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(byteStream)){
            msg = (Message) in.readObject();
        } catch (ClassNotFoundException e) {
            throw new ProtocolException("Class not found", e);
        } catch (IOException e) {
            throw new ProtocolException("Unable to decode binary data.", e);
        }

        return msg;
    }

    @Override
    public byte[] encode(Message msg) throws ProtocolException {
        byte[] bytes = null;

        try (ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(byteStream)){
            out.writeObject(msg);
            bytes = byteStream.toByteArray();
        } catch (IOException e) {
            throw new ProtocolException("Unable to encode Message object", e);
        }

        return bytes;
    }
}
