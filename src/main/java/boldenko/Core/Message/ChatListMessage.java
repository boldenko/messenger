package boldenko.Core.Message;

import boldenko.Core.Storage.Chat;

import java.util.List;

public class ChatListMessage extends Message {
    private long userId;
    private List<Chat> chats;

    public ChatListMessage(long userId, List<Chat> chats) {
        super(Type.MSG_CHAT_LIST);
        this.userId = userId;
        this.chats = chats;
    }

    public List<Chat> chats() {
        return chats;
    }
}
