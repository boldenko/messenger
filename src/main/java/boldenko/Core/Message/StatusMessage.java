package boldenko.Core.Message;

public class StatusMessage extends Message {
    private String info;
    private StatusCode code;



    public enum StatusCode {
        OK,
        CHAT_INVITE,

        BAD_COMMAND,
        BAD_AUTH,
        NOT_AUTH,
        BAD_UID,
        BAD_CHATID,
        NOT_MEMBER,
        LOGIN_OCCUPIED;
    }
    public StatusMessage(StatusCode code) {
        super(Type.MSG_STATUS);
        this.code = code;
    }

    public StatusCode code() {
        return code;
    }

    public StatusMessage(StatusCode code, String info) {
        this(code);
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    @Override
    public String toString() {
        return "[Server]:" + info;
    }
}
