package boldenko.Core.Message;

public class TextMessage extends Message {
    private String text;
    private long chatId;

    public TextMessage(String text, long chatId) {
        super(Type.MSG_TEXT);
        this.text = text;
        this.chatId = chatId;
    }

    public String getText() {
        return text;
    }

    public long getChatId() {
        return chatId;
    }
}
