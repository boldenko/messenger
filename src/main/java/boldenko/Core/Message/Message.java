package boldenko.Core.Message;

import java.io.Serializable;

public abstract class Message implements Serializable {
    private long senderId;
    private long timestamp;
    private Type type;

    // Todo: При добавлении нового типа обновить Client.handle(),
    public enum Type {
        // Клиент-серверу
        MSG_TEXT,
        MSG_LOGIN,
        MSG_CHAT_CREATE,
        MSG_REGISTER,
        MSG_CHAT_LIST_REQUEST,

        // Ответы сервера
        MSG_STATUS,
        MSG_CHAT_LIST
    }

    public Message(Type type) {
        this.type = type;
    }


    public Type getType() {
        return type;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

}
