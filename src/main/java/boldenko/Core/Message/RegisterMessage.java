package boldenko.Core.Message;

public class RegisterMessage extends Message {
    private String login;
    private String password;

    public RegisterMessage(String login, String password) {
        super(Type.MSG_REGISTER);
        this.login = login;
        this.password = password;
    }

    public String login() {return login;}
    public String password() {return password;}

}
