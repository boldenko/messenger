package boldenko.Core.Message;

import boldenko.Core.Storage.Chat;

public class ChatInviteMessage extends StatusMessage {
    private Chat chat;
    public ChatInviteMessage(Chat chat) {
        super(StatusCode.CHAT_INVITE);
        this.chat = chat;
    }

    public Chat getChat() {
        return chat;
    }
}
