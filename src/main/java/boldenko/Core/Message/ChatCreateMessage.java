package boldenko.Core.Message;

import java.util.List;

public class ChatCreateMessage extends Message {
    private List<Long> participants;
    private String title;

    public ChatCreateMessage(String title, List<Long> participants) {
        super(Type.MSG_CHAT_CREATE);
        this.title = title;
        this.participants = participants;
    }

    public List<Long> getParticipants() {
        return participants;
    }

    public String getTitle() {
        return title;
    }
}
