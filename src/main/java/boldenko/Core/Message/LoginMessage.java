package boldenko.Core.Message;

public class LoginMessage extends Message {
    private String login;
    private String password;

    public LoginMessage(String login, String password) {
        super(Type.MSG_LOGIN);
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
