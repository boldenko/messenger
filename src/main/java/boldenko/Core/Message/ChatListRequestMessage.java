package boldenko.Core.Message;

public class ChatListRequestMessage extends Message {
    public ChatListRequestMessage() {
        super(Type.MSG_CHAT_LIST_REQUEST);
    }
}
