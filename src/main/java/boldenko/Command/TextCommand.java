package boldenko.Command;

import boldenko.Core.Message.Message;
import boldenko.Core.Message.StatusMessage;
import boldenko.Core.Message.TextMessage;
import boldenko.Core.Session;
import boldenko.Core.Storage.Chat;
import boldenko.Core.Storage.ChatManager;
import boldenko.Server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TextCommand extends Command{
    private final Logger log = LoggerFactory.getLogger(TextCommand.class);

    public TextCommand(Server server) {
        super(server);
    }

    @Override
    public void execute(Session session, Message message) throws CommandException {
        if (session.user() == null) {
            session.send(new StatusMessage(StatusMessage.StatusCode.NOT_AUTH));
        } else {
            ChatManager chatStorage = server.getChatStorage();
            Chat chat = chatStorage.chat(((TextMessage)message).getChatId());

            if (chat == null) {
                session.send(new StatusMessage(StatusMessage.StatusCode.BAD_CHATID));
            } else
                if (!chat.isMember(session.user().id()))
                    session.send(new StatusMessage(StatusMessage.StatusCode.NOT_MEMBER));
                else {
                    // заполнить оставшиеся поля сообщения
                    TextMessage msg = (TextMessage)message;
                    msg.setSenderId(session.user().id());
                    msg.setTimestamp(System.currentTimeMillis());
                    // Добавить в историю
                    chat.addMessage(msg);
                    log.info("new msg: [" + chat.id() + "] " + chat.title() + ": " + msg.getText());
                    for (Session s : server.getActiveSessions())
                        if (chat.isMember(s.user().id()))
                            s.send(msg);
                    // Переслать всем активным участникам,
                    // неактивные получат его из истории
                }
        }
    }
}
