package boldenko.Command;

import boldenko.Core.Message.Message;
import boldenko.Core.Message.RegisterMessage;
import boldenko.Core.Message.StatusMessage;
import boldenko.Core.Session;
import boldenko.Core.Storage.LoginOccupiedException;
import boldenko.Core.Storage.User;
import boldenko.Server.Server;

public class RegisterCommand extends Command {

    @Override
    public void execute(Session session, Message message) throws CommandException {
        String login = ((RegisterMessage)message).login();
        String password = ((RegisterMessage)message).password();
        User user;
        try {
            user = server.getUserStorage().registerUser(login, password);
            session.setUser(user);
            session.send(new StatusMessage(StatusMessage.StatusCode.OK,
                    "You have been successfully registered. You are logged in as " + login));
        } catch (LoginOccupiedException e) {
            session.send(new StatusMessage(StatusMessage.StatusCode.LOGIN_OCCUPIED));
        }
    }

    public RegisterCommand(Server server) {
        super(server);
    }
}
