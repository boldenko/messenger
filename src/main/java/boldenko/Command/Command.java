package boldenko.Command;

import boldenko.Core.Message.Message;
import boldenko.Core.Session;
import boldenko.Server.Server;

public abstract class Command {
    Server server;

    public Command() {}

    public Command(Server server) {
        this.server = server;
    }

    abstract public void execute(Session session, Message message) throws CommandException;

}
