package boldenko.Command;

import boldenko.Core.Message.ChatCreateMessage;
import boldenko.Core.Message.ChatInviteMessage;
import boldenko.Core.Message.Message;
import boldenko.Core.Message.StatusMessage;
import boldenko.Core.Session;
import boldenko.Core.Storage.Chat;
import boldenko.Core.Storage.User;
import boldenko.Server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ChatCreateCommand extends Command {

    private final Logger log = LoggerFactory.getLogger(ChatCreateCommand.class);

    public ChatCreateCommand(Server server) {
        super(server);
    }

    @Override
    public void execute(Session session, Message message) throws CommandException {
        if (session.user() == null)
            session.send(new StatusMessage(StatusMessage.StatusCode.NOT_AUTH));
        else {
            String title = ((ChatCreateMessage)message).getTitle();
            List<User> party = server.getUserStorage().validate(((ChatCreateMessage)message).getParticipants());
            Chat chat = server.getChatStorage().createChat(title, party);
            chat.addParticipant(session.user());
            session.send(new StatusMessage(StatusMessage.StatusCode.OK, "Chat successfully created."));

            Message invite = new ChatInviteMessage(chat);
            for (Session s : server.getActiveSessions())
                if (chat.isMember(s.user().id()))
                    s.send(invite);

            // Send all participants chat info
        }
    }
}
