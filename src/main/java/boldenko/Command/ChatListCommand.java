package boldenko.Command;

import boldenko.Core.Message.ChatListMessage;
import boldenko.Core.Message.Message;
import boldenko.Core.Message.StatusMessage;
import boldenko.Core.Session;
import boldenko.Core.Storage.ChatManager;
import boldenko.Server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChatListCommand extends Command {
    private final Logger log = LoggerFactory.getLogger(ChatListCommand.class);

    public ChatListCommand(Server server) {
        super(server);
    }

    @Override
    public void execute(Session session, Message message) throws CommandException {
        if (session.user() == null)
            session.send(new StatusMessage(StatusMessage.StatusCode.NOT_AUTH,
                    "You are not authorized to request chat list."));
        else {
            long userId = session.user().id();
            ChatManager mgr = server.getChatStorage();
            session.send(new ChatListMessage(userId, mgr.userChats(userId)));
        }
    }
}
