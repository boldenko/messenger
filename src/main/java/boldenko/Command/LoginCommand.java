package boldenko.Command;

import boldenko.Core.Message.ChatListMessage;
import boldenko.Core.Message.LoginMessage;
import boldenko.Core.Message.Message;
import boldenko.Core.Message.StatusMessage;
import boldenko.Core.Session;
import boldenko.Core.Storage.LoginException;
import boldenko.Core.Storage.UserNotFoundException;
import boldenko.Server.Server;

public class LoginCommand extends Command {
    public LoginCommand(Server server) {
        super(server);
    }

    @Override
    public void execute(Session session, Message message) throws CommandException {
        if (session.user() != null)
            session.send(new StatusMessage(StatusMessage.StatusCode.BAD_AUTH,
                    "You are already logged in."));
        else {
            String login = ((LoginMessage)message).getLogin();
            String password = ((LoginMessage)message).getPassword();
            try {
                session.setUser(server.getUserStorage().login(login, password));
                session.send(new StatusMessage(StatusMessage.StatusCode.OK));

                session.send(new ChatListMessage(session.user().id(),
                        server.getChatStorage().userChats(session.user().id())));
            } catch (LoginException | UserNotFoundException e) {
                session.send(new StatusMessage(StatusMessage.StatusCode.BAD_AUTH, e.getMessage()));
            }
        }
    }
}
