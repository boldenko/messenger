package boldenko.Command;

public class CommandException extends Exception {
    public CommandException(Exception cause) {
        super(cause);
    }

    public CommandException(String info, Exception cause) {
        super(info, cause);
    }

    public CommandException(String info) {
        super(info);
    }
}
