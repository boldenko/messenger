package boldenko.Command;

import boldenko.Core.Message.Message;
import boldenko.Core.Session;

import java.util.HashMap;

// Todo: Implement as Singleton
public class CommandExecutor {
    private HashMap<Message.Type, Command> commands = new HashMap<>();

    public void add(Message.Type type, Command command) {
        commands.put(type, command);
    }

    public Command get(Message.Type type) {
        return commands.get(type);
    }

    public void handle(Message msg, Session session) throws CommandException{
        Message.Type type = msg.getType();
        if (type == null)
            throw new CommandException("Empty message type.");
        else
            if (!commands.containsKey(type))
                throw new CommandException("Invalid message type");
            else
                get(msg.getType()).execute(session, msg);
    }
}
