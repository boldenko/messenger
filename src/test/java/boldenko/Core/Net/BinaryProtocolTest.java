package boldenko.Core.Net;

import boldenko.Core.Message.Message;
import boldenko.Core.Message.TextMessage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class BinaryProtocolTest {
    private Protocol protocol = new BinaryProtocol();

    @Test
    void decodeEncodeTextMessage() {

        Message textMsg = new TextMessage("", 0);

        try {
            Message actual = protocol.decode(protocol.encode(textMsg));

            assertEquals(textMsg.getType(), actual.getType());
            assertEquals(((TextMessage)textMsg).getText(), ((TextMessage)actual).getText() );

        } catch (ProtocolException e) {
            fail("Protocol exception not expected");
        } catch (ClassCastException e) {
            fail("Class cast exception not expected");
        }
    }
}