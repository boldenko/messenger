package boldenko.Core.Storage;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserStorageTest {

    @Test
    void registerUserTest() {
        UserManager storage = new UserStorage();

        try {
            storage.registerUser("admin", "password");
        } catch (LoginOccupiedException e) {
            fail();
        }

        assertEquals(new User(0, "admin"), storage.user(0));
    }

    @Test
    void registerLoginOccupiedTest() throws LoginOccupiedException{
        UserManager storage = new UserStorage();

        storage.registerUser("admin", "password");
        Throwable thrown = assertThrows(LoginOccupiedException.class,
                () -> storage.registerUser("admin", "cocksucker"));

        assertNotNull(thrown);
    }

    @Test
    void invalidIdSelectionTest() {
        UserManager storage = new UserStorage();

        assertNull(storage.user(0));
    }

    @Test
    void validSelectionTest() {
        UserManager storage = new UserStorage();
        try {
            long id = storage.registerUser("admin", "pass").id();
            User expect = new User(id, "admin");
            assertEquals(expect, storage.user(id));

        } catch (LoginOccupiedException e) {
            fail(e.getClass().toString() + " not expected.");
        }
    }

    @Test
    void validLoginTest() {
        UserManager storage = new UserStorage();
        try {
            long id = storage.registerUser("admin", "pass").id();
            User expect = new User(id, "admin");
            assertEquals(expect, storage.login("admin", "pass"));
        } catch (LoginOccupiedException e) {
            fail(e.getClass().toString() + " not expected.");
        } catch (LoginException e) {
            fail(e.getClass().toString() + " not expected.");
        } catch (UserNotFoundException e) {
            fail(e.getClass().toString() + " not expected.");
        }
    }

    @Test
    void invalidLogin_wrongPasswordTest() {
        UserManager storage = new UserStorage();
        try {
            storage.registerUser("admin", "pass");
        } catch (LoginOccupiedException e) {
            fail(e.getClass().toString() + " not expected.");
        }

        Throwable thrown = assertThrows(LoginException.class,
                () -> storage.login("admin", "passssap"));
    }

    @Test
    void invalidLogin_userNotFoundTest() {
        UserManager storage = new UserStorage();
        try {
            storage.registerUser("admin", "pass");
        } catch (LoginOccupiedException e) {
            fail(e.getClass().toString() + " not expected.");
        }

        Throwable thrown = assertThrows(UserNotFoundException.class,
                () -> storage.login("vadmin", "passssap"));
    }

    @Test
    void setAliasTest() {
        UserManager storage = new UserStorage();
        try {
            long id = storage.registerUser("admin", "pass").id();
            String alias = "Dmitry";
            User user = new User(id, alias);
            storage.setAlias(id, alias);
            assertEquals(user, storage.user(id));

            alias = "Floppy";
            user = new User(id, alias);
            storage.setAlias(id, alias);
            assertEquals(user, storage.user(id));

        } catch (LoginOccupiedException e) {
            fail(e.getClass().toString() + " not expected.");
        }
    }

    @Test
    void validationTest() {
        UserManager storage = new UserStorage();
        List<Long> party = new ArrayList<>();

        try {
            long adminId = storage.registerUser("admin", "pass").id();
            party.add(adminId);
            party.add(adminId);
            long billId = storage.registerUser("bill", "dauterive").id();
            party.add(billId);
            party.add(billId);
            party.add(billId);
            party.add((long)1000);
            party.add((long)1020);

            List<Long> expect = new ArrayList<>();
            expect.add(adminId);
            expect.add(billId);

            assertEquals(expect, storage.validate(party));

        } catch (Exception e) {
            fail(e.getClass().toString() + " not expected.");
        }
    }

}