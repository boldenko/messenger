package boldenko.Core.Storage;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ChatStorageTest {

    @Test
    void emptyStorageTest() {
        ChatManager manager = new ChatStorage();

        List<Chat> expect = new ArrayList<>();
            assertNull(manager.chat(0));
    }

    @Test
    void createChat() {
        ChatManager manager = new ChatStorage();
        List<Long> participants = new ArrayList<>();
        participants.add((long)0);
        participants.add((long)1);
        participants.add((long)2);

        //UserManager userManager = new Use

        long chatId;
        //Chat newChat = manager.createChat("Job", participants);
        //List<Long> actual = newChat.participants();
        //assertEquals(participants, actual);

    }

    @Test
    void getWrongIdChat() {
        ChatManager manager = new ChatStorage();

        manager.createChat("Test", new ArrayList<>());
        assertNull(manager.chat(1488));
    }
}